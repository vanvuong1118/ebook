<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    //
    public function index(){
        return view('client.checkout.index');
    }
    public function payment_methods (){
        return view('client.checkout.payment_method');
    }
}
