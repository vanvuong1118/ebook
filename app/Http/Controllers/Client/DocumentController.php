<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    //
//    protected $_data = ['title'=>'Tài liệu'];
    public function index(){
//        $this->_data['item']='Book Page';
        return view('client.document.index');
    }
    public function generate_pdf(){
        return view('client.document.document_pdf');
    }
}
