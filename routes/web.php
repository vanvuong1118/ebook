<?php

use App\Http\Controllers\Client\CategoriesController;
use App\Http\Controllers\Client\CheckoutController;
use App\Http\Controllers\Client\DocumentController;
use App\Http\Controllers\Client\Home\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//home
Route::any('/',[HomeController::class,'index'])->name('client.home.index');


//categories
Route::any('/categories',[CategoriesController::class,'index'])->name('client.categories.index');


//document
Route::group(['prefix'=>'document'],function(){
   Route::any('/',[DocumentController::class,'index'])->name('client.document.index');
   Route::any('/pdf',[DocumentController::class,'generate_pdf'])->name('client.document.pdf');
});

//checkout

Route::group(['prefix' => 'checkout'],function(){
    Route::get('/',[CheckoutController::class,'index'])->name('client.checkout.index');
    Route::any('/payment-methods',[CheckoutController::class,'payment_methods'])->name('client.checkout.paymentMethods');
});
