@extends('client.layout.layout')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card book-detail">
                    <div class="iq-card-body p-0">
                        <iframe src="{{asset('storage/asset/images/book/book.pdf')}}"  style="width: 100%; height: 90vh;"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
