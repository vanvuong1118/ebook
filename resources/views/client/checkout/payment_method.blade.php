@extends('client.layout.layout')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <div class="iq-card">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">Please choose your payment methodgi</h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <ul class="nav nav-pills mb-3 nav-fill" id="pills-tab-1" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab-fill" data-toggle="pill" href="#pills-home-fill" role="tab" aria-controls="pills-home" aria-selected="true"><img
                                        src="{{asset('storage/asset/images/checkout/momo.png')}}"class="mr-3 avatar-50" alt=""></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab-fill" data-toggle="pill" href="#pills-profile-fill" role="tab" aria-controls="pills-profile" aria-selected="false"><img
                                        src="{{asset('storage/asset/images/checkout/vnpay.png')}}"class="mr-3 avatar-50" alt=""></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab-fill" data-toggle="pill" href="#pills-contact-fill" role="tab" aria-controls="pills-contact" aria-selected="false"><img
                                        src="{{asset('storage/asset/images/checkout/paypal.jpg')}}"class="mr-3 avatar-50" alt=""></a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent-1">
                            <div class="tab-pane fade show active" id="pills-home-fill" role="tabpanel" aria-labelledby="pills-home-tab-fill">
                                <div class="container">
                                    <div class="row justify-content-center align-items-center mt-5">
                                        <div class="col-lg-6 col-md-8 text-center">
                                            <h2 class="mb-3">Thanh toán đơn giản với Momo</h2>
                                            <p class="lead">Sử dụng ứng dụng Momo để thanh toán hóa đơn, mua sắm và chuyển tiền dễ dàng và nhanh chóng.</p>
                                            <div class="d-flex justify-content-center">
                                                <div class="mr-3">
                                                    <a href="#"><img src="{{asset('storage/asset/images/checkout/momo.png')}}"class="mr-3 avatar-80"  alt="Ví Momo" width="100"></a>
                                                </div>
                                                <div>
                                                    <h4>Ví điện tử Momo</h4>
                                                    <p>Thanh toán bằng ví điện tử Momo với nhiều ưu đãi hấp dẫn.</p>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-center mt-3">
                                                <div class="mr-3">
                                                    <a href="#"><img src="{{asset('storage/asset/images/checkout/qr-code-momo.png')}}"class="mr-3 avatar-80"  alt="Momo Pay" width="100"></a>
                                                </div>
                                                <div>
                                                    <h4>Momo Pay</h4>
                                                    <p>Tải về và thanh toán trực tuyến với Momo Pay và nhận ngay ưu đãi đặc biệt.</p>
                                                </div>
                                            </div>
                                            <a class="btn btn-primary btn-lg mt-3" href="#" role="button">Tải ứng dụng Momo</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-profile-fill" role="tabpanel" aria-labelledby="pills-profile-tab-fill">
                                <div class="container">
                                    <div class="row justify-content-center align-items-center mt-5">
                                        <div class="col-lg-6 col-md-8 text-center">
                                            <h2 class="mb-3">Thanh toán đơn giản với VNPAY</h2>
                                            <p class="lead">Sử dụng VNPAY để thanh toán hóa đơn, mua sắm và chuyển tiền dễ dàng và nhanh chóng.</p>
                                            <div class="d-flex justify-content-center">
                                                <div class="mr-3">
                                                    <a href="#"><img src="{{asset('storage/asset/images/checkout/vnpay.png')}}"class="mr-3 avatar-80" alt="VNPAY" width="100"></a>
                                                </div>
                                                <div>
                                                    <h4>Ví điện tử VNPAY</h4>
                                                    <p>Thanh toán bằng ví điện tử VNPAY với nhiều ưu đãi hấp dẫn.</p>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-center mt-3">
                                                <div class="mr-3">
                                                    <a href="#"><img src="{{asset('storage/asset/images/checkout/qr_vnpay.jpg')}}"class="mr-3 avatar-80" alt="VNPAY QR" width="100"></a>
                                                </div>
                                                <div>
                                                    <h4>VNPAY QR</h4>
                                                    <p>Thanh toán trực tuyến với VNPAY QR và nhận ngay ưu đãi đặc biệt.</p>
                                                </div>
                                            </div>
                                            <a class="btn btn-primary btn-lg mt-3" href="#" role="button">Tải ứng dụng VNPAY</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="pills-contact-fill" role="tabpanel" aria-labelledby="pills-contact-tab-fill">
                                <div class="container">
                                    <div class="row justify-content-center align-items-center mt-5">
                                        <div class="col-lg-6 col-md-8 text-center">
                                            <h2 class="mb-3">Thanh toán đơn giản với PayPal</h2>
                                            <p class="lead">Sử dụng PayPal để thanh toán hóa đơn, mua sắm và chuyển tiền dễ dàng và nhanh chóng.</p>
                                            <div class="d-flex justify-content-center">
                                                <div class="mr-3">
                                                    <a href="#"><img src="{{asset('storage/asset/images/checkout/paypal.jpg')}}"class="mr-3 avatar-80" alt="Paypal" width="100">
                                                    </a>
                                                </div>
                                                <div>
                                                    <h4>PayPal</h4>
                                                    <p>Thanh toán bằng PayPal với nhiều ưu đãi hấp dẫn.</p>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-center mt-3">
                                                <div class="mr-3">
                                                    <a href="#"><img src="{{asset('storage/asset/images/checkout/qr_paypal.png')}}"class="mr-3 avatar-80" alt="Paypal QR" width="100"></a>
                                                </div>
                                                <div>
                                                    <h4>PayPal QR</h4>
                                                    <p>Thanh toán trực tuyến với PayPal QR và nhận ngay ưu đãi đặc biệt.</p>
                                                </div>
                                            </div>
                                            <p class="mt-3">Với PayPal, bạn có thể thanh toán cho hàng hóa và dịch vụ trên toàn thế giới, chuyển tiền cho bạn bè và người thân, hay nhận thanh toán từ khách hàng của mình.</p>
                                            <a class="btn btn-primary btn-lg mt-3" href="#" role="button">Đăng ký tài khoản PayPal</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
