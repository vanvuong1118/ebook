@extends('client.layout.layout')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height iq-mb-3">
                    <img src="{{asset('storage/asset/images/page-img/07.jpg')}}" class="card-img-top" alt="#">
                    <div class="iq-card-body">
                        <h4 class="card-title">240$ / 1 Month</h4>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias amet asperiores at, atque consequuntur cupiditate expedita impedit libero maiores nemo nesciunt non officia perspiciatis quod similique velit, voluptatem. Incidunt, unde.</p>
                        <a href="{{route('client.checkout.paymentMethods')}}" class="btn btn-primary btn-block">Pay</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height iq-mb-3">
                    <img src="{{asset('storage/asset/images/page-img/07.jpg')}}" class="card-img-top" alt="#">
                    <div class="iq-card-body">
                        <h4 class="card-title">620$ / 3 Months</h4>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias amet asperiores at, atque consequuntur cupiditate expedita impedit libero maiores nemo nesciunt non officia perspiciatis quod similique velit, voluptatem. Incidunt, unde.</p>
                        <a href="{{route('client.checkout.paymentMethods')}}" class="btn btn-primary btn-block">Pay</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height iq-mb-3">
                    <img src="{{asset('storage/asset/images/page-img/07.jpg')}}" class="card-img-top" alt="#">
                    <div class="iq-card-body">
                        <h4 class="card-title">1800$ / 9 Months</h4>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias amet asperiores at, atque consequuntur cupiditate expedita impedit libero maiores nemo nesciunt non officia perspiciatis quod similique velit, voluptatem. Incidunt, unde.</p>
                        <a href="{{route('client.checkout.paymentMethods')}}" class="btn btn-primary btn-block">Pay</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height iq-mb-3">
                    <img src="{{asset('storage/asset/images/page-img/07.jpg')}}" class="card-img-top" alt="#">
                    <div class="iq-card-body">
                        <h4 class="card-title">2400$ / 1 Year</h4>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias amet asperiores at, atque consequuntur cupiditate expedita impedit libero maiores nemo nesciunt non officia perspiciatis quod similique velit, voluptatem. Incidunt, unde.</p>
                        <a href="{{route('client.checkout.paymentMethods')}}" class="btn btn-primary btn-block">Pay</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
